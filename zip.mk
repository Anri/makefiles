ZIP = zip -r
RM  = rm

IGNORE = "*/build/*" "*/.gradle/*" "*/.idea/*" "*/local.properties" "*/*.pdf" \
         "*/*.out" "*/bin/*"

EXTRANAME =
EXTRAFILE =


all: help

help:
	@echo "make archive"
	@echo "  -> Compresse tout les TP"
	@echo ""
	@echo "make archive10"
	@echo "  -> Compresse le TP 10"
	@echo ""
	@echo "make archives NUMS='1 2'"
	@echo "  -> Compresse les TPs 1 et 2"

archive%:
	$(ZIP) "TP$(*)$(EXTRANAME).zip" "TP$(*)" "$(EXTRAFILE)" -x $(IGNORE)

archive:
	$(ZIP) "TPs$(EXTRANAME).zip" TP* "$(EXTRAFILE)" -x $(IGNORE)


# Trick https://stackoverflow.com/a/1542661
noop  =
space = $(noop) $(noop)

NUMS := 1 2 3
archives:
	$(ZIP) "TP$(subst $(space),-,$(NUMS))$(EXTRANAME).zip" \
	  $(foreach n,$(NUMS),TP$(n)) "$(EXTRAFILE)" -x $(IGNORE)

clean:
	$(RM) $(wildcard TP*.zip)
