
SYNC  = unison
MKDIR = mkdir -p

SMB_APPDATA = /mnt/z

HOME_WIN = /mnt/c/Users/anri
HOME_WSL = /home/anri
HOME_SMB = /mnt/y/Anri

DIR = $(HOME_WSL)/Perso


.PHONY: cv site site-blog site-contacts site-portfolio youtube wifi

all:


GJ=Incognito
gamejam:
	@$(SYNC) "$(HOME_WIN)/Documents/$(GJ)" "$(DIR)/$(GJ)"


cv:
	@$(SYNC) "$(HOME_SMB)/Boulot/CV" "$(DIR)/cv"


site:
	@$(MKDIR) site
	@$(SYNC) "$(SMB_APPDATA)/site/data/index.md" "$(DIR)/site/index.md"
	@$(SYNC) "$(SMB_APPDATA)/site/data/index-fr.md" "$(DIR)/site/index-fr.md"
	$(MAKE) site-blog
	$(MAKE) site-contacts
	$(MAKE) site-portfolio

site-blog:
	@$(SYNC) "$(SMB_APPDATA)/site/data/blog" "$(DIR)/site/blog"

site-contacts:
	@$(SYNC) "$(SMB_APPDATA)/site/data/contacts" "$(DIR)/site/networks"

site-portfolio:
	@$(SYNC) "$(SMB_APPDATA)/site/data/projects" "$(DIR)/site/portfolio"


youtube:
	@$(SYNC) "$(HOME_SMB)/Streaming/YouTube" "$(DIR)/YouTube"


wifi:
	@$(SYNC) "$(HOME_SMB)/Autres/wifi" "$(DIR)/wifi"
