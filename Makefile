VC = git
CP = cp -rv

PERSO = ~/Perso
UNIV  = ~/Univ


.PHONY: sync

main:

sync:
	@$(VC) pull
	@$(CP) perso.mk $(PERSO)/Makefile
	@$(CP) univ.mk    $(UNIV)/Makefile
