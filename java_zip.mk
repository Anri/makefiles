RM    = rm -rf
TAR   = tar -czf
CP    = cp -r
MKDIR = mkdir -p
PASTE = xclip -o

SRC_DIR = src
BIN_DIR = bin

JAVA_ARGS  = --enable-preview -enableassertions
JAVAC_ARGS = -Xlint:unchecked

ENTRY := App
ARGV  :=

ARCHIVE_NAME = nom
ARCHIVE_EXT  = tar.gz
EXTRA_FILES :=


run: compilation
run:
	java $(JAVA_ARGS) --class-path $(BIN_DIR) $(ENTRY) $(ARGV)

compilation:
	find $(SRC_DIR) -name "*.java" -print | xargs javac $(JAVAC_ARGS) -d $(BIN_DIR)

paste:
	mkdir -p $(SRC_DIR)
	$(PASTE) > $(SRC_DIR)/Main.java

all:
	run

clean:
	$(RM) $(BIN_DIR) "$(ARCHIVE_NAME).$(ARCHIVE_EXT)"

archive:
	$(MKDIR) "$(ARCHIVE_NAME)"
	$(CP) $(EXTRA_FILES) "$(SRC_DIR)" "README.txt" "$(ARCHIVE_NAME)"
	$(TAR) "$(ARCHIVE_NAME).$(ARCHIVE_EXT)" "$(ARCHIVE_NAME)"
	$(RM) "$(ARCHIVE_NAME)"
