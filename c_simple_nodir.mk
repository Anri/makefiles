CC = gcc
RM = rm

CFLAGS = -std=gnu17 -pedantic -Wall -Wextra -Wshadow -Wcast-align \
         -Wstrict-prototypes -fanalyzer -fsanitize=undefined -g -Og \
         -fsanitize=leak

# Même nom que les .c
EXES = prog1 prog2 prog3 # nom des programmes (avec un main)
DEPS = dependance1.o dependance2.o # dépendances pour tous les programmes
EXT  = .out # je met une extension pour le filtre de sync unison

all: $(EXES)

$(EXES): %: %.c $(DEPS)
	$(CC) $^ -o $@$(EXT) $(CFLAGS)

%.o: %.c
	$(CC) -c $< -o $@ $(CFLAGS)

clean:
	@$(RM) *.o $(addsuffix $(EXT), $(EXES)) 2>/dev/null |:
