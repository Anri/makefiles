

SYNC  = unison
MKDIR = mkdir -p

HOME_WIN  = /mnt/c/Users/anri
HOME_WSL  = /home/anri
COURS_SMB = $(HOME_WSL)/quatriemeSemestre

UNIV = $(HOME_WSL)/Univ

.SILENT:
.PHONY: java heptagon edf anglais repartie expose

main:


java: TP_NUM = TP3
java:
	$(SYNC) "$(COURS_SMB)/progObjet/TPs/$(TP_NUM)" "$(UNIV)/java_tp"

heptagon: TP_NUM = TP1
heptagon:
	$(SYNC) "$(COURS_SMB)/progSynchrone/TPs/$(TP_NUM)" "$(UNIV)/ept_tp"

edf: TP_NUM = TP4
edf:
	$(SYNC) "$(COURS_SMB)/infoEmbarquee/TPs/$(TP_NUM)" "$(UNIV)/edf"

anglais:
	$(SYNC) "$(COURS_SMB)/anglais/presentation" "$(UNIV)/anglais"

repartie:
	$(SYNC) "$(COURS_SMB)/progRepar/TPs" "$(UNIV)/repartie"

expose:
	$(SYNC) "$(COURS_SMB)/progComp/snd-presentation" "$(UNIV)/expose"

rapport:
	$(SYNC) "$(COURS_SMB)/progComp/snd-rapport" "$(UNIV)/rapport-expose"
