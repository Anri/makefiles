CC = ocamlopt
RM = rm -rf

NAME = main
EXE  = a.out
LIB  = unix.cmxa -I +unix


all: run

compilation:
	$(CC) -o $(EXE) $(LIB) $(NAME).ml

run: compilation
	./$(EXE)

EXTS = cmi cmx o
clean:
	$(RM) $(EXE) $(foreach ext,$(EXTS),$(NAME).$(ext))
