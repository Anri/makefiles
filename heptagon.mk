PASTE = xclip -o
CAT   = bat -pp

FILE  ?= test
ENTRY ?= test
LIMIT ?= 5


all: test

# Possible usage: make testn ENTRY=node0
test: pastefile eval

# Usage: make testn LIMIT=10
testn: pastefile evaln

pastefile:
	$(PASTE) > $(FILE).ept

eval:
	hept -ts $(ENTRY) $(FILE).ept

evaln:
	hept -ts $(ENTRY) -m $(LIMIT) $(FILE).ept

print:
	heptc -i -target c $(FILE).ept
	$(CAT) $(FILE)_c/$(FILE).h
	$(CAT) $(FILE)_c/$(FILE).c
